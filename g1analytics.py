# To host on a server, launch with command : streamlit run g1analytics.py --server.port=8501 --browser.serverPort='8501' --browser.serverAddress='gviz.analysons.com'
# And add a vhost conf like this :
#<VirtualHost *:80>
#    ServerName gviz.analysons.com
#    <IfModule mod_rewrite.c>
#    RewriteEngine on
#    RewriteCond %{SERVER_PORT} !^443$
#    RewriteRule ^/(.*) https://%{SERVER_NAME}/$1 [L,R]
#    </IfModule>
#
#</VirtualHost>
#
#<VirtualHost *:443>
#   ServerName gviz.analysons.com
#
#   RewriteEngine On
#   RewriteCond %{HTTP:Upgrade} =websocket
#   RewriteRule /(.*) ws://localhost:8501/$1 [P]
#   RewriteCond %{HTTP:Upgrade} !=websocket
#   RewriteRule /(.*) http://localhost:8501/$1 [P]
#   ProxyPassReverse / http://localhost:8501
#
#    SSLCertificateFile "/etc/letsencrypt/live/gviz.analysons.com/cert.pem"
#    SSLCertificateKeyFile "/etc/letsencrypt/live/gviz.analysons.com/privkey.pem"
#</VirtualHost>


import streamlit as st

st.set_page_config(
    page_title="G1 analytics",
    page_icon="📊",
    layout="wide",
)

def _max_width_():
    print('Called')
    max_width_str = f"max-width: 20000px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>    
    """,
        unsafe_allow_html=True,
    )
    
#_max_width_() # Does not work
 
st.sidebar.markdown("# Ğ1 Analytics")

st.markdown("# 📊 Bienvenue sur Ğ1 Analytics")
st.write("Naviguez via le menu sur la gauche (cliquez sur la petite flèche > en haut à gauche pour ouvrir le panneau)")
st.write("Donnez à la clé ci-dessous pour contribuer au développement :")
st.code('FCHCfCD3uz8k29thHvr2pPgYZwibuP4noHVwTEPu5Aka')



