# 2 pb pour la key de Lucas : 1/ les "trous" dans le non UD  2/ les labels du non UD ne sont pas alignés avec les UD (qui n'ont pas de trou)
# Il faut agir sur les données pandas agrégées au mois : remplacer tous les trous par des 0 sur toute la plage de dates où il y a DU et non DU
# => Résolu depuis

import streamlit as st

import pandas as pd
import numpy as np
import os
from datetime import timedelta
import dateutil.relativedelta
from datetime import date
from datetime import datetime

from matplotlib import pyplot as plt

pd.options.display.max_columns = 2000
pd.options.display.max_rows = 2000
pd.options.display.float_format = '{:,}'.format # Thousand separator

import matplotlib as mpl
import matplotlib.ticker as plticker

st.set_page_config(
    page_title="Transactions",
    page_icon="📊",
    layout="wide",
)

st.sidebar.markdown("# Transactions page")


DATA_EXPORT_PATH = '/home/francois/coding/g1/g1analytics/transactions/data'
TXS_PATH = os.path.join(DATA_EXPORT_PATH, 'g1analytics_txs_with_ud.csv')
BALANCE_PATH = os.path.join(DATA_EXPORT_PATH, 'g1analytics_balances.csv')
MEMBERS_PATH = os.path.join(DATA_EXPORT_PATH, 'g1analytics_members.csv')
LAST_UPDATE_PATH = os.path.join(DATA_EXPORT_PATH,'last_update.txt')

@st.cache
def load_data(date_colname):
	print('Cache update')
	
	df_txs_detail = pd.read_csv(TXS_PATH, dtype={'block_number':int,
             'is_UD': bool,
             'comment': str,
             'issuer_key': str,
             'recipient_key': str,
             'amount': int,
             'month': str,
                                             
	}, parse_dates=['blockstampTime', 'time'])
	
	#last_month = df_txs_detail['blockstampTime'].max().to_period('M').to_timestamp()
	
	df_balance = pd.read_csv(BALANCE_PATH, dtype={'block_number':int,
             'is_UD': bool,
             'comment': str,
             'pkey': str,
             'partner_pkey': str,
             'amount': int,
             'month': str,
                                             
	}, parse_dates=['blockstampTime', 'time'])

	df_agg_month_recipient = df_txs_detail.groupby(['month', 'recipient_key', 'is_UD']).agg({'amount': lambda x : sum(x)/100}).unstack('is_UD').fillna(0)
	df_agg_month_recipient.columns = df_agg_month_recipient.columns.droplevel(0)
	df_agg_month_recipient['Total'] = df_agg_month_recipient[True] + df_agg_month_recipient[False]
	df_agg_month_recipient['percent_without_UD'] = (df_agg_month_recipient[False] / df_agg_month_recipient['Total'])*100

	df_members = pd.read_csv(MEMBERS_PATH, names=['pkey', 'is_member'])
	
	
	#df_ud_received_by_member = df_balance.query('is_UD == True').query('blockstampTime < @last_month').groupby(['pkey'])['amount'].sum()
	#df_spent_by_user = df_balance.query('amount < 0').query('blockstampTime < @last_month').groupby(['pkey'])['amount'].sum()
	df_ud_received_by_member = df_balance.query('is_UD == True').groupby(['pkey'])['amount'].sum()
	df_spent_by_user = df_balance.query('amount < 0').groupby(['pkey'])['amount'].sum()

	df_ud_vs_spent = pd.merge(df_ud_received_by_member, 
		 df_spent_by_user, 
		 how='left', 
		 left_index=True, 
		 right_index=True, 
		 suffixes=('_UD_received', '_G1_spent')).fillna(0)

	df_remaining_ud = (df_ud_vs_spent['amount_UD_received'] + df_ud_vs_spent['amount_G1_spent']).clip(0) / 100

	with open(LAST_UPDATE_PATH, "r") as fp:
	    last_update_text = fp.readline()
		
	return last_update_text, df_txs_detail, df_balance, df_agg_month_recipient, df_members, df_remaining_ud


def main():
	st.title("📊 Ğ1 Transactions page")

	# We add hours=3 timedelta because our update script ends a little before 2 am every day
	yesterday = date.today() - timedelta(days=1) - timedelta(hours=3)
	yesterday_colname = yesterday.strftime('%m/%d/%y').lstrip('0').replace("/0", "/")

	last_update_text, df_txs_detail, df_balance, df_agg_month_recipient, df_members, df_remaining_ud = load_data(yesterday_colname)
	st.sidebar.write(f"Last update: {last_update_text}")
	
	item_choice = st.sidebar.selectbox('Choisir les données à afficher', ('Graphes', 'Détail des transactions', 'Analyse des montants extrêmes', 'Informations globales'))
	

	plot_UD = st.sidebar.checkbox('Afficher les données DU', value=True)

	filter_key = st.sidebar.text_input('Saisissez votre clé pour des visualisations personnalisées', '')

	if (item_choice == 'Graphes'):

		########## Transactions number
		font = {'size'   : 20,
	       'family' : 'DejaVu Sans' }

		mpl.rc('font', **font)

		PERCENT_QUANTILE = 0.995

		last_month = df_txs_detail['blockstampTime'].max().to_period('M').to_timestamp()
		total_number_of_transactions = df_txs_detail.shape[0]

		#fig = plt.figure(figsize=(20,10))
		fig,ax = plt.subplots(figsize=(20,10))


		if (filter_key != ''):
			df_with_ud = df_txs_detail.query('is_UD == True').query('(recipient_key == @filter_key | issuer_key == @filter_key) & blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].count()
			df_without_ud = df_txs_detail.query('is_UD == False').query('(recipient_key == @filter_key | issuer_key == @filter_key) & blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].count()

			if (df_with_ud.shape[0] == 0) and (df_without_ud.shape[0] == 0):
				st.write('No data for this key')
				st.stop()
			
			# Need to handle when ud and non ud data have gaps, or different number of xticks
			elif (df_with_ud.shape[0] == 0):
				idx_wide = pd.period_range(min(df_without_ud.index), max(df_without_ud.index))
				
			elif (df_without_ud.shape[0] == 0):
				idx_wide = pd.period_range(min(df_with_ud.index), max(df_with_ud.index))
							
			else:
				idx_wide = pd.period_range(min(min(df_without_ud.index), min(df_with_ud.index)), 
						   max(max(df_without_ud.index), max(df_with_ud.index)))

			df_with_ud = df_with_ud.reindex(idx_wide, fill_value=0)
			df_without_ud = df_without_ud.reindex(idx_wide, fill_value=0)

		else:
			df_with_ud = df_txs_detail.query('is_UD == True').query('blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].count()
			df_without_ud = df_txs_detail.query('is_UD == False').query('blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].count()


		if (plot_UD == True):
		    ax.plot(df_with_ud.index.astype(str), 
			     df_with_ud, 
			     label='UD', linewidth=3, marker='o')
			     	     
		    #ax.set_xticks(np.arange(df_with_ud.shape[0] + offset_ud_nonud)) # If uncomment, then need to apply offset
		    #ax.set_xticklabels([''] * offset_ud_nonud + list(df_with_ud.index.astype(str)), rotation = 45)
		    #ax.set_xticklabels([''] * offset_ud_nonud + list(df_with_ud.index.astype(str)))
		    
		    #for label in ax.xaxis.get_ticklabels()[::3]:
		    #	label.set_visible(False)

		ax.plot(df_without_ud.index.astype(str), df_without_ud, label='Non UD', linewidth=3, marker='s')
			 
		#ax.set_xticks(np.arange(df_without_ud.shape[0] + offset_nonud_ud))
		#ax.set_xticklabels([''] * offset_nonud_ud + list(df_without_ud.index.astype(str)), rotation = 45)
		#ax.set_xticklabels([''] * offset_nonud_ud + list(df_without_ud.index.astype(str)))


		#loc = plticker.MultipleLocator(base=3.0) # this locator puts ticks at regular intervals
		#ax.xaxis.set_major_locator(loc)

		#for label in ax.xaxis.get_ticklabels()[::3]:
		#    label.set_visible(True)

		#for label in ax.xaxis.get_ticklabels()[::3]:
		#    label.set_visible(False)

		#plt.setp(ax.get_xticklabels(), visible=False)
		#plt.setp(ax.get_xticklabels()[::5], visible=True)
		
		
		plt.xticks(np.arange(0, max(df_without_ud.shape[0], df_with_ud.shape[0]), 3)) # Display every 3 x ticks labels

		ax.get_yaxis().set_major_formatter(  # Format without scientific notation (which is matplotlib default)
		    mpl.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))

		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.spines['bottom'].set_visible(False)
		ax.spines['left'].set_visible(False)

		ax.annotate('francoislibre@proton.me\nSource: Duniter V1 data', xy=(1, -0.25), xytext=(-15, 10), fontsize=10,
		xycoords='axes fraction', textcoords='offset points',
		bbox=dict(facecolor='white', alpha=0.8),
		horizontalalignment='right', verticalalignment='bottom')

		plt.xticks(rotation = 45, ha='right', rotation_mode='anchor') 
		    
		plt.legend(loc='upper left')
		plt.grid()
		
		title_str = 'Ğ1 number of transactions'
		
		if (filter_key != ''):
			title_str = title_str + f'\n key: {filter_key}'
			
		plt.title(title_str, fontweight='bold')
		st.pyplot(fig)


		#st.dataframe(df_without_ud)
		#st.dataframe(df_with_ud)	
		#st.dataframe(df_without_ud.index.astype(str))
		
		

		########## Transactions amount
		
		


		font = {'size'   : 20,
		       'family' : 'DejaVu Sans' }

		mpl.rc('font', **font)

		PERCENT_QUANTILE = 0.995
		
		last_month = df_txs_detail['blockstampTime'].max().to_period('M').to_timestamp()
		outlier_amount_cap = df_txs_detail.query('is_UD == False')['amount'].quantile(PERCENT_QUANTILE)
		total_number_of_transactions = df_txs_detail.shape[0]

		fig,ax = plt.subplots(figsize=(20,10))

		if (filter_key != ''):
			df_with_ud = df_txs_detail.query('is_UD == True').query('(recipient_key == @filter_key | issuer_key == @filter_key) & amount < @outlier_amount_cap and blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].sum()/100
			df_without_ud_incoming = df_txs_detail.query('is_UD == False').query('(recipient_key == @filter_key) & amount < @outlier_amount_cap and blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].sum()/100
			df_without_ud_outcoming = -df_txs_detail.query('is_UD == False').query('(issuer_key == @filter_key) & amount < @outlier_amount_cap and blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].sum()/100



			if (df_with_ud.shape[0] == 0) and (df_without_ud_incoming.shape[0] == 0) and (df_without_ud_outcoming.shape[0] == 0):
				st.write('No data for this key')
				st.stop()
			
			# Need to handle when ud and non ud data have gaps, or different number of xticks
			first_period = min([v for v in [min(df_with_ud.index, default=None), min(df_without_ud_incoming.index, default=None), min(df_without_ud_outcoming.index, default=None)] if v is not None])
			last_period = max([v for v in [max(df_with_ud.index, default=None), max(df_without_ud_incoming.index, default=None), max(df_without_ud_outcoming.index, default=None)] if v is not None])
			
			idx_wide = pd.period_range(first_period, last_period)

			df_with_ud = df_with_ud.reindex(idx_wide, fill_value=0)
			df_without_ud_incoming = df_without_ud_incoming.reindex(idx_wide, fill_value=0)
			df_without_ud_outcoming = df_without_ud_outcoming.reindex(idx_wide, fill_value=0)

		else:
			df_with_ud = df_txs_detail.query('is_UD == True').query('amount < @outlier_amount_cap and blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].sum()/100
			df_without_ud = df_txs_detail.query('is_UD == False').query('amount < @outlier_amount_cap and blockstampTime < @last_month').groupby(df_txs_detail['blockstampTime'].dt.to_period('M'))['amount'].sum()/100

		if (plot_UD == True):
		    ax.plot(df_with_ud.index.astype(str), 
			     df_with_ud, 
			     label='UD', linewidth=3, marker='o')
			     
		    #ax.set_xticklabels(list(df_with_ud.index.astype(str)), rotation = 45)

		if (filter_key == ''):
			ax.plot(df_without_ud.index.astype(str), 
				 df_without_ud, 
				 label='Non UD', linewidth=3, marker='s')
				 
			plt.xticks(np.arange(0, max(df_without_ud.shape[0], df_with_ud.shape[0]), 3)) # Display every 3 x ticks labels

		else:
			ax.plot(df_without_ud_incoming.index.astype(str), 
				 df_without_ud_incoming, 
				 label='Non UD incoming', linewidth=3, marker='s')

			ax.plot(df_without_ud_outcoming.index.astype(str), 
				 df_without_ud_outcoming, 
				 label='Non UD outcoming', linewidth=3, marker='v')

			plt.xticks(np.arange(0, max(df_without_ud_incoming.shape[0], df_without_ud_outcoming.shape[0], df_with_ud.shape[0]), 3)) # Display every 3 x ticks labels
		

		#ax.set_xticklabels(list(df_without_ud.index.astype(str)), rotation = 45)

		#loc = plticker.MultipleLocator(base=3.0) # this locator puts ticks at regular intervals
		#ax.xaxis.set_major_locator(loc)

		ax.get_yaxis().set_major_formatter(  # Format without scientific notation (which is matplotlib default)
		    mpl.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))

		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.spines['bottom'].set_visible(False)
		ax.spines['left'].set_visible(False)

		ax.annotate('francoislibre@proton.me\nSource: Duniter V1 data', xy=(1, -0.25), xytext=(-15, 10), fontsize=10,
		xycoords='axes fraction', textcoords='offset points',
		bbox=dict(facecolor='white', alpha=0.8),
		horizontalalignment='right', verticalalignment='bottom')

		plt.xticks(rotation = 45) 
		    
		plt.legend(loc='upper left')
		plt.grid()

		title_str = f'Ğ1 amount of transactions\n(Excluded amounts over {PERCENT_QUANTILE*100}% for readability)'
		if (filter_key != ''):
			title_str = title_str + f'\n key: {filter_key}'

		plt.title(title_str, fontweight='bold')
		#plt.show()
		st.pyplot(fig)
		plt.figure().clear()
		plt.close()
		st.write(f"For better readability, outlier amounts > {PERCENT_QUANTILE*100}% of all amounts are excluded")
		st.write(f"Number of transactions excluded: {df_txs_detail.query('amount > @outlier_amount_cap').shape[0]:,} over a total of {total_number_of_transactions:,}")

		
		#################
		
		# A noter qu'une personne peut avoir un compte membre + des comptes portefeuille que l'on ne sait pas rattacher automatiquement au compte membre
		# Mais sur la moyenne globale, tous les comptes sont comptabilisés donc cela se compense

		font = {'size'   : 20,
		       'family' : 'DejaVu Sans' }

		mpl.rc('font', **font)

		#FILTER_KEY = '9yji8DtwFVhS3QtgdUJQEzJakcNfXV8RpRVwnSJ4szaY' # Lucas
		#FILTER_KEY = 'FCHCfCD3uz8k29thHvr2pPgYZwibuP4noHVwTEPu5Aka' # Francois	
		#FILTER_KEY = 'ArPjsLndxatcyG9joLf1XVi23KjXeiAJGeyMBnD3Fzrp' # Account without UD
		#FILTER_KEY = 'B3aAp1agRum87AzyBhRY6KgGqh98LSajx1Jd2k6ru4To' # Ma Aude
		#FILTER_KEY = '7TCAWZ79HozNojF9oc9V3P1C3fqgZbN91xxy4recMUkU' # Reumy
		#FILTER_KEY = '11AiC9N1iu4QAqLk8oNLA5EPtrzY7kRa2QedfwqhA8b'
		#ACTIVATE_FILTER_KEY = False


		last_month = df_txs_detail['blockstampTime'].max().to_period('M').to_timestamp()
		last_month_string = str(last_month.year) + '-' + str(last_month.month).zfill(2)

		#fig = plt.figure(figsize=(20,10))
		fig,ax = plt.subplots(figsize=(20,10))

		if (filter_key != ''):
			df_plot = df_agg_month_recipient\
				.query('recipient_key == @filter_key')\
				.query('month < @last_month_string')\
				.groupby(['month'])\
				.agg({'percent_without_UD': np.mean})['percent_without_UD']\
				#.ewm(6).mean() # 6 months exponentially weighted mean
			
			df_plot.index = pd.to_datetime(df_plot.index).to_period('M')

			if (df_plot.shape[0] == 0):
				st.write('No incoming transaction data for this key')
				st.stop()
							
			else:
				idx_wide = pd.period_range(min(df_plot.index), 
						   max(df_plot.index))
						   
				print(idx_wide)

			df_plot = df_plot.reindex(idx_wide, fill_value=0)

		else:
		    df_plot = df_agg_month_recipient\
				.query('month < @last_month_string')\
				.groupby(['month'])\
				.agg({'percent_without_UD': np.mean})['percent_without_UD']
				
		    df_plot.index = pd.to_datetime(df_plot.index).to_period('M')

		ax.plot(df_plot.index.astype(str), 
			     df_plot, 
			     label='Percent without UD', linewidth=3, marker='o')

		#ax.set_xticklabels(list(df_plot.index.astype(str)), rotation = 45)

		#loc = plticker.MultipleLocator(base=3.0) # this locator puts ticks at regular intervals
		#ax.xaxis.set_major_locator(loc)
		plt.xticks(np.arange(0, max(df_without_ud.shape[0], df_with_ud.shape[0]), 3)) # Display every 3 x ticks labels

		ax.get_yaxis().set_major_formatter(  # Format without scientific notation (which is matplotlib default)
		    mpl.ticker.FuncFormatter(lambda x, p: format(int(x), ',')))

		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.spines['bottom'].set_visible(False)
		ax.spines['left'].set_visible(False)

		ax.annotate('francoislibre@proton.me\nSource: Duniter V1 data', xy=(1, -0.25), xytext=(-15, 10), fontsize=10,
		xycoords='axes fraction', textcoords='offset points',
		bbox=dict(facecolor='white', alpha=0.8),
		horizontalalignment='right', verticalalignment='bottom')

		plt.xticks(rotation = 45) 
		    
		plt.legend(loc='upper left')
		plt.grid()
		title_str = f'Ğ1 mean percentage of income transactions without UD vs total'
		if (filter_key != ''):
		    title_str = title_str + f'\n key: {filter_key}'
		    
		plt.title(title_str, fontweight='bold')
		#plt.show()
		st.pyplot(fig)
		plt.figure().clear()
		plt.close()

	elif (item_choice == 'Détail des transactions'):
		if (filter_key != ''):
			st.write(f'Transactions de la clé {filter_key}')
			
			outcome_amount = df_txs_detail.query('issuer_key == @filter_key')['amount'].sum()/100
			income_amount = df_txs_detail.query('recipient_key == @filter_key')['amount'].sum()/100
			income_amount_ud = df_txs_detail.query('recipient_key == @filter_key & is_UD == True')['amount'].sum()/100
			income_amount_nonud = df_txs_detail.query('recipient_key == @filter_key & is_UD == False')['amount'].sum()/100
			balance = (df_txs_detail.query('recipient_key == @filter_key')['amount'].sum() - df_txs_detail.query('issuer_key == @filter_key')['amount'].sum())/100
			balance2 = df_balance.query("pkey == @filter_key")['amount'].sum() / 100

			global_income_nonud_vs_total = (income_amount_nonud / (income_amount))*100

			st.write(f'Outcome amount : {outcome_amount}')
			st.write(f'Income amount : {income_amount}')
			st.write(f'>From which UD : {income_amount_ud}')
			st.write(f'>From which non UD : {income_amount_nonud}')
			st.write(f'Balance: {balance}')
			st.write(f'Global income non UD vs total: {global_income_nonud_vs_total:.2f}%')
			
			df_balance_perso = df_balance.query('pkey == @filter_key').sort_values(by=['blockstampTime'])[['is_UD', 'comment', 'time', 'partner_pkey', 'amount', 'block_number']]
			df_balance_perso['amount'] = df_balance_perso['amount'] / 100
			#st.dataframe(df_balance.query('pkey == @filter_key').sort_values(by=['blockstampTime'])[['is_UD', 'comment', 'time', 'partner_pkey', 'amount', 'block_number']])
			st.dataframe(df_balance_perso.style.format({'amount': '{:.2f}'}))
			#st.dataframe(df_balance_perso.style.format("{:.2f}"))
		else:
			st.write('Saisissez une clé dans le panneau sur la gauche')
			
			
	elif (item_choice == 'Analyse des montants extrêmes'):
		st.write("Dans les graphes de montants de transaction, nous avons masqué les montants extrêmes ci-dessous, afin de ne pas perturber les visuels : si on les affichait, cela déséquilibrerait totalement les graphes, alors qu'il ne s'agit que d'une faible minorité de transactions")
		outlier_amount_cap = df_txs_detail.query('is_UD == False')['amount'].quantile(0.995)

		st.write(f"Total number of transactions: {df_txs_detail.shape[0]:,}")

		st.write(f"Number of transactions with amount > 99.5% of amounts: {df_txs_detail.query('amount > @outlier_amount_cap').shape[0]:,}")

		st.write('Top outliers by amount')
		st.dataframe(df_txs_detail.query('amount > @outlier_amount_cap').sort_values(by=['amount'], ascending=False))

	elif (item_choice == 'Informations globales'):
		df_agg_recipient = df_txs_detail.query('is_UD == False').groupby(['recipient_key']).agg({'amount': [lambda x : sum(x)/100, 'count']}).fillna(0)
		df_agg_recipient.columns = df_agg_recipient.columns.droplevel(0)
		df_agg_recipient_members = df_agg_recipient.join(df_members.set_index('pkey')).fillna(0).query('is_member == 1')
		
		st.write(f"Monetary mass in circulation (= sum of all received UD since {str(df_txs_detail['blockstampTime'].min())}):")
		st.write(f" {df_txs_detail.query('is_UD == True')['amount'].sum()/100:,}")
		
		number_of_members = df_members.query('is_member == 1').shape[0]
		st.write(f'Number of members: {number_of_members}')        
		
		number_of_former_members = df_members.query('is_member == 0').shape[0]
		st.write(f'Number of users that have been member but are not anymore: {number_of_former_members}')
        
		nb_users_1_transaction_or_more = df_agg_recipient.shape[0]
		nb_members_1_transaction_or_more = df_agg_recipient_members.shape[0]
		st.write(f"{nb_users_1_transaction_or_more} users have made at least 1 receiving transaction (UD not included) since the beginning of G1")
		st.write(f"{nb_members_1_transaction_or_more} members (among current members) have made at least 1 receiving transaction (UD not included) since the beginning of G1")
		st.write(f"Percent of members that never made any transaction (UD not included): {100 - ((nb_members_1_transaction_or_more / number_of_members)*100): .2f}%")
		
		nb_users_2_transactions_or_more = df_agg_recipient.query('count > 1').shape[0]
		nb_users_2_transactions_or_more_percent = (nb_users_2_transactions_or_more / nb_users_1_transaction_or_more)*100
		
		nb_members_2_transactions_or_more = df_agg_recipient_members.query('count > 1').shape[0]
		nb_members_2_transactions_or_more_percent = (nb_members_2_transactions_or_more / nb_members_1_transaction_or_more)*100

		
		st.write(f"{nb_users_2_transactions_or_more} users have made at least 2 receiving transactions (UD not included) since the beginning of G1 which is only {nb_users_2_transactions_or_more_percent:.2f}% of users having made their first transaction")
		st.write(f"{nb_members_2_transactions_or_more} members have made at least 2 receiving transactions since the beginning of G1 which is only {nb_members_2_transactions_or_more_percent:.2f}% of members having made their first transaction")
		
		df_agg_balances = df_balance.query('is_UD == False').groupby(['pkey']).agg({'amount': [lambda x : sum(x)/100, 'count']}).fillna(0)
		df_agg_balances.columns = df_agg_balances.columns.droplevel(0)
		df_agg_balances = df_agg_balances.reset_index(drop=False)

		nb_users_1_transaction_or_more = df_agg_balances.shape[0]
		st.write(f"{nb_users_1_transaction_or_more} users have made at least 1 transaction (receiving OR sending, UD not included) since the beginning of G1")

		nb_users_2_transactions_or_more = df_agg_balances.query('count > 1').shape[0]
		nb_users_2_transactions_or_more_percent = (nb_users_2_transactions_or_more / nb_users_1_transaction_or_more)*100
		st.write(f"{nb_users_2_transactions_or_more} users have made at least 2 transactions (receiving OR sending, UD not included) since the beginning of G1 which is {nb_users_2_transactions_or_more_percent:.2f}% of users having made their first transaction")

		NB_MONTHS_ACTIVE = 6
		NB_TRANSACTIONS_CONSIDERED_ACTIVE = 6
		last_month = df_txs_detail['blockstampTime'].max().to_period('M').to_timestamp()
		a_few_months_ago = last_month - dateutil.relativedelta.relativedelta(months=NB_MONTHS_ACTIVE)

		df_balance_recent_nonud = df_balance.query('blockstampTime >= @a_few_months_ago & is_UD == False')\
		.groupby(['pkey']).agg({'amount': [lambda x : sum(x)/100, 'count']})

		df_balance_recent_nonud.columns = df_balance_recent_nonud.columns.droplevel(0)
		df_balance_recent_nonud = df_balance_recent_nonud.reset_index(drop=False)

		df_balance_recent_nonud = df_balance_recent_nonud.query('count >= @NB_TRANSACTIONS_CONSIDERED_ACTIVE') # count is the number of transactions

		active_pkeys = list(df_balance_recent_nonud['pkey'].unique())

		st.write(f'Number of active users (having at least {NB_TRANSACTIONS_CONSIDERED_ACTIVE} non-UD transaction in the last {NB_MONTHS_ACTIVE} months)')
		st.write(f'{len(active_pkeys)}')		

		df_balance_active = df_balance.query('pkey in @active_pkeys')
		percent_active = (len(df_balance_active['pkey'].unique()) / len(df_balance['pkey'].unique()))*100
		st.write(f'Active users represent {percent_active:.2f}% of total')
		
		
		# This calculation is an approximation, it may be underestimated.
		# Because, for example, members that send all their received UD to another account they own => this is considered as used UD
		remaning_ud_amount = df_remaining_ud.sum()
		nb_members_or_former = df_remaining_ud.shape[0]

		st.write(f'Remaining UD amount : not spent by members and former members: {remaning_ud_amount:,}')
		st.write('(formula : UD amount not spent by a member = floor(Total UD received by member - Total G1 spent by member, 0) )')

		st.write(f'=> Public key with the highest remaining UD: {df_remaining_ud.sort_values(ascending=False).index[0]} : amount : {df_remaining_ud.sort_values(ascending=False).values[0]:,}' )

if (__name__ == '__main__'):
	main()

