# -*- coding:utf-8 -*-
#This script generates CSV files containing :
# All G1 transactions since the beginning, including Universal Dividend
# All G1 account balances
# Required data sources for this script :
# Duniter V1 running node endpoint url that exposes block numbers being UD transactions : URL_UD_BLOCK_NUMBERS
# Duniter V1 SQLlite transactions file (txs.db) : DB_PATH
# Duniter V1 levelDB files : DB_PATH_BLOCKCHAIN
# Wot Wizard active endpoint : WOTWIZARD_ENDPOINT

import sqlite3
import pandas as pd
import os
from datetime import timedelta
import dateutil.relativedelta
from datetime import date
from datetime import datetime
import time
import ast

from python_graphql_client import GraphqlClient
import json, requests
from tqdm import tqdm

# If wotwizard is down, find another endpoint at : https://ginspecte.mithril.re/service_types/6
#WOTWIZARD_ENDPOINT = 'https://wwgql.coinduf.eu'
WOTWIZARD_ENDPOINT = 'https://gql.wotwizard.trentesaux.fr'
URL_UD_BLOCK_NUMBERS = 'https://g1.analysons.com/blockchain/with/ud'

import leveldb
DB_PATH_BLOCKCHAIN = '/home/francois/coding/g1/g1analytics/transactions/leveldb/level_blockchain'

DB_PATH = '/home/francois/coding/g1/duniter_db_test/'
DB_FILE_TXS = os.path.join(DB_PATH, 'txs.db')
SCRIPT_PATH = '/home/francois/coding/g1/g1analytics/transactions/'
DATA_EXPORT_PATH = '/home/francois/coding/g1/g1analytics/transactions/data'

LOG_PATH = os.path.join(SCRIPT_PATH, 'logs', 'update_transactions_data.log')

import logging
logging.basicConfig(level=logging.DEBUG, filename=LOG_PATH, filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

import time
start_time = time.time()

logging.info('Start of update transactions data')

connection_txs = sqlite3.connect(DB_FILE_TXS)
cursor = connection_txs.cursor()

# Load transactions
logging.info('Load and parse transactions from SQLite DB')
df_txs = pd.read_sql_query("SELECT * from txs", connection_txs, parse_dates=['time', 'blockstampTime'])

variables = {'block_number':str(),
             'is_UD': bool(),
             'comment': str(),
             'blockstampTime': datetime,
             'time': datetime,
             'issuer_key': str(),
             'recipient_key': str(),
             'amount': int(),
}

df_txs_detail = pd.DataFrame(variables, index=[])

progbar = tqdm(range(len(df_txs)))

txs_detail = []

for index, row in df_txs.iterrows():
    assert(len(ast.literal_eval(df_txs.iloc[0]['issuers'])) == 1) # Error if not only 1 issuer

    for line_output in ast.literal_eval(df_txs.iloc[index]['outputs']):
        if ast.literal_eval(df_txs.iloc[index]['issuers'])[0] != line_output.split(':')[2].split('SIG(')[1][:-1]:
            # Only add transaction line if issuer != recipient
            txs_detail.append([df_txs.iloc[index]['block_number'],
                               False,
                               df_txs.iloc[index]['comment'],
                               df_txs.iloc[index]['blockstampTime'],
                               df_txs.iloc[index]['time'],
                               ast.literal_eval(df_txs.iloc[index]['issuers'])[0] ,
                               line_output.split(':')[2].split('SIG(')[1][:-1],
                               int(line_output.split(':')[0]),
            ])

    progbar.update(1)

progbar.refresh();

df_txs_detail = pd.DataFrame(txs_detail, columns=df_txs_detail.columns)
df_txs_detail['blockstampTime'] = pd.to_datetime(df_txs_detail['blockstampTime'], unit='s')
df_txs_detail['time'] = pd.to_datetime(df_txs_detail['time'], unit='s')

# Delete lines with null block number (probably unvalidated transactions in node memory or something like that)
df_txs_detail = df_txs_detail.dropna(axis=0, subset=['block_number'], how='any')

# Convert to int because, if there were null block numbers, that caused column type to be float (since int does not accept NA)
df_txs_detail['block_number'] = df_txs_detail['block_number'].astype(int)

# Get UD rights (in / out) from wot wizard
## Instantiate the client with an endpoint.
logging.info('Load and parse UD in/out rights from wotwizard endpoint')
client = GraphqlClient(endpoint=WOTWIZARD_ENDPOINT)

query = """{
    idSearch(with: {status_list: [REVOKED, MISSING, MEMBER]}) {
        ids {
            uid
            pubkey
            history {
                in
                block {
                    number
                }
            }
        }
    }
}
    """

ww_in_outs = client.execute(query=query)

with open(os.path.join(DATA_EXPORT_PATH, 'ww_in_outs_query_result.json'), 'w') as fp:
    json.dump(ww_in_outs, fp)

#In case wotwizard is down, you can replace above code by this :
#with open('/home/francois/coding/g1/g1analytics/transactions/data/ww_in_outs_query_result.json', 'r') as ww_file:
#    ww_in_outs = json.load(ww_file)

valid_UD_intervals = {}

for user_entry in ww_in_outs['data']['idSearch']['ids']:
    pkey = user_entry['pubkey']

    #logging.info(pkey)
    in_out_interval_list = []
    in_out_interval_indice = 0

    #logging.info([block_num['block']['number'] for block_num in user_entry['history']])

    user_in_out_list = user_entry['history']
    for in_out_interval_indice in range(len(user_in_out_list)):
        if in_out_interval_indice < len(user_in_out_list)-1:


            in_out_interval_list.append([user_in_out_list[in_out_interval_indice]['block']['number'],
                                         user_in_out_list[in_out_interval_indice+1]['block']['number'],
                                         user_in_out_list[in_out_interval_indice]['in']
                                        ])

    in_out_interval_list.append([user_in_out_list[-1]['block']['number'],
                                 999999999,
                                 user_in_out_list[-1]['in']
                                ])

    valid_UD_interval_list = [in_out_interval_list_elem for in_out_interval_list_elem in in_out_interval_list if in_out_interval_list_elem[2] == True]

    valid_UD_intervals[pkey] = valid_UD_interval_list

# Dictionary of members (1 = currently member,  0 = non member)
members_dict = {}

for pkey in valid_UD_intervals:
    if valid_UD_intervals[pkey][-1][1] == 999999999:
        members_dict[pkey] = 1

    else:
        members_dict[pkey] = 0

with open(os.path.join(DATA_EXPORT_PATH, 'valid_UD_intervals.json'), 'w') as fp:
    json.dump(valid_UD_intervals, fp)

#� Calculate and load UD rights

url_response = requests.get(URL_UD_BLOCK_NUMBERS)
ud_block_numbers_json = url_response.text
ud_block_numbers_list = ast.literal_eval(ud_block_numbers_json)['result']['blocks']

logging.info(f"{len(ud_block_numbers_list)} blocks have Universal Dividend which corresponds to {len(ud_block_numbers_list)/365} years")

##� Load UDs and blocks from LevelDB
logging.info('Load UD blocks from LevelDB and add UD transactions')
db_blockchain = leveldb.LevelDB(DB_PATH_BLOCKCHAIN)

progbar = tqdm(range(len(ud_block_numbers_list)))

ud_transactions = []

for ud_block_number in ud_block_numbers_list:
    try:
        block_response = ast.literal_eval(
                str(db_blockchain.Get(bytes(str(ud_block_number).zfill(10), encoding='utf-8')))[12:-2].replace('true', 'True').replace('false', 'False')
            )
        ## Above code only works when there is a UD in block (which is always the case because we only loop through UD blocks). If no UD, would need to convert "null" to None for literal_eval to work

    except:  # If block not found, it probably means it's dated from current day, whereas our blockchain leveldb files are not updated in real time
        continue

    for pkey in valid_UD_intervals:
        ud_right = False
        for valid_UD_interval in valid_UD_intervals[pkey]:
            if ud_block_number >= valid_UD_interval[0] and ud_block_number < valid_UD_interval[1]:
                ud_right = True

        if ud_right == True:
            ud_transactions.append([ud_block_number,
                        True,
                        'Universal Dividend',
                        block_response['time'],
                        block_response['time'],
                        '',
                        pkey,
                        block_response['dividend'],
                       ])

    progbar.update(1)

progbar.refresh();

##� Load UDs in our transactions dataframe
logging.info('Consolidate transactions and balance data')
df_ud_transactions = pd.DataFrame(ud_transactions, columns=df_txs_detail.columns)
df_ud_transactions['blockstampTime'] = pd.to_datetime(df_ud_transactions['blockstampTime'], unit='s')
df_ud_transactions['time'] = pd.to_datetime(df_ud_transactions['time'], unit='s')
df_txs_detail = pd.concat([df_txs_detail, df_ud_transactions], axis=0).reset_index(drop=True)
df_txs_detail['month'] = df_txs_detail['blockstampTime'].dt.to_period('M')

df_balance_issuers = df_txs_detail[['block_number', 'is_UD', 'comment', 'blockstampTime', 'time',
       'issuer_key', 'recipient_key', 'amount', 'month']].copy(deep=True)
df_balance_issuers['amount'] = -df_balance_issuers['amount']
df_balance_issuers = df_balance_issuers.rename(columns={'issuer_key': 'pkey'})
df_balance_issuers = df_balance_issuers.rename(columns={'recipient_key': 'partner_pkey'})

df_balance_recipients = df_txs_detail[['block_number', 'is_UD', 'comment', 'blockstampTime', 'time',
       'recipient_key', 'issuer_key', 'amount', 'month']].copy(deep=True)

df_balance_recipients = df_balance_recipients.rename(columns={'recipient_key': 'pkey'})
df_balance_recipients = df_balance_recipients.rename(columns={'issuer_key': 'partner_pkey'})

df_balance = pd.concat([df_balance_recipients, df_balance_issuers], axis=0).reset_index(drop=True)

assert(df_balance_recipients.shape[0] + df_balance_recipients.shape[0] == df_balance.shape[0])

assert(df_balance['amount'].sum() == 0)
# Total balance is 0 because :
# non UD transactions compensate each other
# UD transactions appear positively for receving pkeys, and negatively with empty pkeys :  they compensate each others too

logging.info('Balances loaded. Monteray mass:')
logging.info(-df_balance[df_balance['pkey'] == '']['amount'].sum()/100)

# Export transactions data
logging.info('Export transactions and balance data')
df_txs_detail.to_csv(os.path.join(DATA_EXPORT_PATH, 'g1analytics_txs_with_ud.csv'), index=False)
df_balance.to_csv(os.path.join(DATA_EXPORT_PATH, 'g1analytics_balances.csv'), index=False)

# Export members data
logging.info('Export members data')
pd.Series(members_dict).to_csv(os.path.join(DATA_EXPORT_PATH, 'g1analytics_members.csv'), header=False)

with open(os.path.join(DATA_EXPORT_PATH, 'last_update.txt'), 'w') as fp:
    print(datetime.fromtimestamp(time.time()), file=fp)

logging.info(f"End. Execution time : {(time.time() - start_time):.2f} seconds")

